import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    constructor(props) {
        super(props);
        const boardSize = +this.props.boardSize;
        this.state = {
            squares: Array(boardSize * boardSize).fill(null),
            xIsNext: true,
            boardSize: boardSize || this.props.minBoardSize || 3,
            coordinate: null
        };
    }

    render() {
        const winner = checkWinner(this.state.squares, this.state.coordinate, this.state.boardSize);
        let status;

        if(winner) {
            status = 'Выиграл: ' + winner;
        } else {
            status = 'Следующий ход: ' + this.whoIsNext();
        }

        return (
            <div>
                <div className="status">{status}</div>
                {this.renderInput()}
                {this.renderBoard()}
            </div>
        );
    }

    handleClick(i) {
        const squares = this.state.squares.slice();
        if(checkWinner(squares, i, this.state.boardSize) || squares[i]) {
            return;
        }
        squares[i] = this.whoIsNext();
        this.setState({
            squares: squares,
            xIsNext: !this.state.xIsNext,
            coordinate: i
        });
    }

    renderBoard() {
        let board = [];
        for(let rowId = 0; rowId < this.state.boardSize; rowId++) {
            board.push(
                <div
                    className="board-row"
                    key={rowId}
                >
                    {this.renderBoardRow(rowId)}
                </div>
            );
        }
        return board;
    }

    renderBoardRow(rowId) {
        let square = [];
        for(let colId = 0; colId < this.state.boardSize; colId++) {
            let squareId = rowId * this.state.boardSize + colId;
            square.push( this.renderSquare(squareId) );
        }
        return square;
    }

    renderSquare(i) {
        return (
            <Square
                key={i}
                value={this.state.squares[i]}
                onClick={() => this.handleClick(i)}
            />
        );
    }

    renderInput() {
        if(this.props.showSizeChanger) {
            return <input type="range" min={this.props.minBoardSize} max={this.props.maxBoardSize} onInput={this.updateBoardSize.bind(this)} />
        }
    }

    whoIsNext() {
        return this.state.xIsNext ? 'X' : 'O';
    }

    updateBoardSize(e) {
        let boardSize = e.target.value;
        this.setState({
            squares: Array(boardSize * boardSize).fill(null),
            xIsNext: true,
            boardSize: boardSize,
        })
    }
}

class Game extends React.Component {
    render() {
        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        boardSize="9"
                        maxBoardSize="9"
                        minBoardSize="3"
                        showSizeChanger={true}
                    />
                </div>
                <div className="game-info">
                    <div>{/* status */}</div>
                    <ol>{/* TODO */}</ol>
                </div>
            </div>
        );
    }
}

function calculateWinner(squares) {

    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}

function checkWinner( squares, coordinate, boardSize,  ) {
    let row = Math.floor(coordinate/boardSize);
    let col = coordinate/boardSize;
    if(/\d./i.test(col.toString())) {
        col = Math.floor((col) * 10) / 10;
        col = col.toString().replace(/\d./i, "");
        col = +col;
    } else col = col * 0;

    if(col !== 0 && +col !== 8 && row !== 0 && row !== 8) {

        console.log(row);
        console.log(col);

    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
